-module (practise).
-export ([sortListDesc/1, cleanList/2, removeZeros/1, addDandT/1]).



% 1. Ordena la llista en sentit descendent
sortListDesc (L) ->
    lists:reverse(lists:sort(L)).


% 2. Treure tots els elements imparells i tambe els majors que el numero del argument 2
cleanList(L, NMAX) ->
    lists:filter(fun(X) -> ((X rem 2) == 0) and (X < NMAX) end, L).


% 3. Remove elements than finish on 0
removeZeros (L) ->
    lists:filter(fun(X) -> (testZero(X) == false) end, L).

testZero (H) ->
    if (H < 10) ->
        if (H == 0) -> true;
        true -> false
        end;
    true ->
        testZero(H - 10)
    end.


% 4. Ficar de cada numero el seu doble i el seu triple
addDandT([H|T]) ->
    [H, 2*H, 3*H] ++ addDandT(T);
addDandT([]) ->
    [].
